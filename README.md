# Introduction

py-tkz-tab est un programme distribué sous licence GPLv3 ou ultérieure.

Il a pour objectif de réaliser le tableau de variations d'une fonction en utilisant sympy et tikz, graĉe au paquet
tkz-tab d'Alain Matthes.

# Utilisation

## Dans un fichier TeX

Il peut être directement intégré dans le fichier Tex en utilisant pythontex :

```text
\documentclass[a4paper]{article}
\usepackage[]{pythontex}
\usepackage{tkz-tab}

\begin{document}
\begin{pycode}
from pyTkzTab import tabvar
print(tabvar('sqrt(x**2-1)', limites=True, extrema=True, lgt=3, espcl=3))
\end{pycode}
\end{document}
```

Ce qui permet d'obtenir après compilation :

![Tableau de variations de f(x)=\sqrt(x^2-1)](./images/with_pythontex.png?raw=true "Tableau de variations de sqrt(x**2-1)")

## Dans une console

On peut aussi utiliser le programme dans une console afin de modifier le résultat obtenu :

```python
from pyTkzTab import tabvar

print(tabvar('x**3+3*x**2+1', limites=True, extrema=True, D='Interval(-5, 5)'))
```

ce qui permet d'obtenir :

```text
\[ f(x) =  x^{3} + 3 x^{2} + 1 \qquad f'(x)=3 x \left(x + 2\right)\]
\begin{tikzpicture}
  \tkzTabInit[lgt=2,espcl=2,deltacl=0.5]
  {$x$/.6,$3 x$/.6,$x + 2$/.6,$f'(x)$/.6, $f(x)$/1.2}
  {$-5$,$-2$,$0$,$5$}
  \tkzTabLine{,-,t,-,z,+,}
  \tkzTabLine{,-,z,+,t,+,}
  \tkzTabLine{,+,z,-,z,+,}
  \tkzTabVar{-/$-49$,+/$5$,-/$1$,+/$201$}
\end{tikzpicture}
```

On peut ensuite obtenir l'image en compilant le document suivant :

```text
\documentclass[crop,tikz]{standalone}
\usepackage{tkz-tab}
\begin{document}
\begin{tikzpicture}
  \tkzTabInit[lgt=2,espcl=2,deltacl=0.5]
  {$x$/.6,$3 x$/.6,$x + 2$/.6,$f'(x)$/.6, $f(x)$/1.2}
  {$-5$,$-2$,$0$,$5$}
  \tkzTabLine{,-,t,-,z,+,}
  \tkzTabLine{,-,z,+,t,+,}
  \tkzTabLine{,+,z,-,z,+,}
  \tkzTabVar{-/$-49$,+/$5$,-/$1$,+/$201$}
\end{tikzpicture}
\end{document}
```

Et voilà :

![Tableau de vartiations de f(x)=x3+3x2+1 sur -5, 5](./images/standalone.png?raw=true "Tableau de variations de x^3+3x^2+1")

# Développement

La documentation se trouve ici : https://py-tkz-tab.frama.io/py-tkz-tab/
