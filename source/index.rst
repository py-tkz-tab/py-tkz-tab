.. pytabvar documentation master file, created by
   sphinx-quickstart on Thu Dec 17 09:23:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pytabvar's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pyTkzTab

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
