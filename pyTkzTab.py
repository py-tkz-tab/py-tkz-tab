#!/usr/bin/env python3

# Copyright (C) 2020-2020 -- Jérôme Ortais (jerome.ortais@pyromaths.org)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Étudie les variations d'une fonction à partir du signe de sa dérivée

#. calcule la dérivée et la factorise
#. recherche l'ensemble de définition de f
#. dans l'adhérence de cet intervalle, cherche les racines de chacun des facteurs et précise si f′ est définie en \
    cette valeur
#. pour chaque intervalle compris entre deux racines consécutives, cherche le signe de chaque facteur et de f′
#. précise les variations de f sur chacun des intervalles précédents
#. met en forme les informations afin de les rendre compatibles avec tkz-tab

Le tableau obtenu avant mise en forme ressemble à ceci :

.. code-block:: python

    [
    {'value': -oo, 'fp': ',-,', 'f': '+/$0$,', '$-1$': ',-,', '$x^{2}$': ',+,'},
    {'value': 0, 'fp': 'd,-,', 'f': '-D+/$-\\infty$/$\\infty$,', '$-1$': 't,-,', '$x^{2}$': 'z,+,'},
    {'value': oo, 'fp': '', 'f': '-/$0$', '$-1$': '', '$x^{2}$': ''},
    ]

Chaque dictionnaire comporte :

* l'étiquette 'value' avec une borne d'intervalle (qui peut être une racine ou une valeur interdite),
* une étiquette 'f' qui donne l'extremum et la position (+ ou -) de la flèche au niveau de la borne 'value',
* une étiquette 'fp' qui donne le signe de la dérivée sur l'intervalle ayant pour borne inférieure 'value',
* des étiquettes au format tex de chacun des facteurs de la dérivée avec le signe comme pour 'fp'.

"""

from typing import Union, List, Tuple, Dict, Any, Literal

from sympy import diff, Symbol, calculus, S, oo, fraction, limit, latex, Eq, solve, simplify
from sympy import sqrt, exp, log, cos, sin
# from sympy import sqrt, exp, log, cos, sin
from sympy.core import Mul
from sympy.core.numbers import Exp1
from sympy.functions import exp
from sympy.sets import sets, Reals
from sympy.core.function import Function


def zeroExiste(racine: float, tableau: List[Dict[str, Any]]) -> Tuple[bool, int]:
    """
    Retourne la position du nombre racine dans tableau

    :param racine: racine à insérer
    :param tableau: voir introduction
    :return: un booléen disant si la zero existe déjà dans tableau, et un entier disant à quelle place l'insérer
    """
    pos = 0
    for i in range(len(tableau)):
        if tableau[i]["value"] == racine:
            return True, i
        if tableau[i]["value"] < racine:
            pos = i + 1
    return False, pos


def insertRacines(fonction: Function, tableau: List[dict], dfacteurs: dict, Df: sets.Set, zero: Literal['z', 'd'],
                  latex_parameters: Dict[str, Any]) -> List[Dict[str, Any]]:
    """

    :param fonction: la fonction pour laquelle on cherche les racines
    :param tableau: voir introduction
    :param dfacteurs: Le dictionnaire contenant les facteurs de f′
    :param Df: L'ensemble de définition de f
    :param zero: 'z' si la racine est un zéro de f′, 'd' si c'est une valeur interdite
    :param latex_parameters: dictionnaire contenant les paramètres d'affichage LaTeX
    :return: tableau mis à jour avec les éventuelles racines trouvées
    """
    x = Symbol('x')
    # Workaround: solveset ne trouve pas de racines pour sqrt((x-1)*(x+1)) == 0, contrairement à solve, qui n'est
    # pourtant pas conseillé dans la documentation : https://docs.sympy.org/latest/modules/solvers/solveset.html
    equation = Eq(fonction, 0)
    racines = solve(equation, x)
    if not racines:
        return tableau
    else:
        for i in range(len(racines) - 1, -1, -1):
            if not racines[i].is_real or not Df.closure.contains(racines[i]):
                racines.pop(i)
        if not racines:
            return tableau
    for r in racines:
        existe, place = zeroExiste(r, tableau)
        if existe:
            tableau[place][latex(fonction, **latex_parameters)] = 'z,'
            tableau[place]['fp'] = zero + ','
        else:
            # N'insert la valeur que si elle est dans Df
            if tableau[0]['value'] < r < tableau[-1]['value']:
                dic = {'value': r, 'fp': zero + ',', 'f': '', }
                dic.update(dfacteurs)
                dic[latex(fonction, **latex_parameters)] = 'z,'
                tableau.insert(place, dic)
    return tableau


def ensembleDeDefinition(intervalles: sets.Set, dfacteurs: Dict[str, str]) -> List[Dict[str, Any]]:
    """
    Crée la liste de dictionnaires contenant les bornes de l'ensemble de définition de f.

    :param intervalles: Les intervalles sur lesquels f est définie
    :param dfacteurs: Le dictionnaire contenant les facteurs de f′
    :return: le tableau initialisé
    """
    if type(intervalles) == sets.Interval or intervalles == Reals:
        dic = {'value': intervalles.start, 'fp': '', 'f': '', }
        dic.update(dfacteurs)
        ld = [dic]
        dic = {'value': intervalles.end, 'fp': '', 'f': '', }
        dic.update(dfacteurs)
        ld.append(dic)
        return ld
    else:
        ld = []
        for intervalle in list(intervalles.args):
            ldb = ensembleDeDefinition(intervalle, dfacteurs)
            for elb in ldb:
                doublon = False
                for el in ld:
                    if el['value'] == elb['value']:
                        doublon = True
                if not doublon:
                    ld.append(elb)
        return ld


def definitSignes(facteur: Function, tableau: List[Dict[str, Any]], Df: sets.Set, fp: Function,
                  latex_parameters: Dict[str, Any]) -> List[Dict[str, Any]]:
    """
    Cherche le signe des expressions sur chacun des intervalles trouvés.
    Le fait par tests car solve_univariate_inequality ne sait résoudre f′(x)>0 pour f(x)=sqrt(x**2-1)

    :param facteur: un des facteurs de f′, ou f′
    :param tableau: voir introduction
    :param Df: ensemble de définition de f
    :param fp: f′
    :param latex_parameters: dictionnaire contenant les paramètres d'affichage LaTeX
    :return: tableau mis à jour avec les éventuelles racines trouvées
    """
    x = Symbol('x')
    if facteur == fp:
        clef = 'fp'
    else:
        clef = latex(facteur, **latex_parameters)
    Dg = calculus.util.continuous_domain(facteur, x, Df.closure)
    for i in range(len(tableau) - 1):
        borneInf, borneSup = tableau[i]['value'], tableau[i + 1]['value']
        if sets.Interval(borneInf, borneSup, left_open=True, right_open=True).is_subset(Df):
            centre = centreIntervalle(borneInf, borneSup)
            if tableau[i][clef] == '':
                if Dg.contains(borneInf):
                    if i == 0:
                        tableau[i][clef] = ','
                    else:
                        tableau[i][clef] += 't,'
                elif borneInf == -oo:
                    tableau[i][clef] = ','
                else:
                    tableau[i][clef] = 'd,'
            if facteur.subs(x, centre) >= 0:
                tableau[i][clef] += '+,'
            else:
                tableau[i][clef] += '-,'
        else:
            if tableau[i][clef] == '':
                if tableau[i][clef] == 'z,':
                    tableau[i][clef] = 'z,h,'
                elif Dg.contains(borneInf):
                    tableau[i][clef] = 't,h,'
                else:
                    tableau[i][clef] = 'd,h,'
            else:
                tableau[i][clef] += 'h,'
    borneSup = tableau[-1]['value']
    if borneSup != oo and not Dg.contains(borneSup):
        tableau[-1][clef] = 'd'
    elif tableau[-1][clef] == 'z,':
        tableau[-1][clef] = 'z'
    return tableau


def centreIntervalle(borneInf: float, borneSup: float) -> float:
    """Renvoie une valeur comprise dans l'intervalle ]borneInf, borneSup[ afin de pouvoir tester le signe de
    l'expression sur cet intervalle

    >>> centreIntervalle(-oo, 0)
    -1
    >>> centreIntervalle(-4, 0)
    -2.0
    >>> centreIntervalle(3, oo)
    4
    >>> centreIntervalle(-oo, oo)
    0

    """
    if borneInf == -oo:
        if borneSup == oo:
            centre = 0.
        else:
            centre = borneSup - 1
    elif borneSup == oo:
        centre = borneInf + 1
    else:
        centre = (borneInf + borneSup) / 2
    return centre


def definitVariations(tableau: List[Dict[str, Any]], f: Function, Df: sets.Set, limites: bool, extrema: bool,
                      latex_parameters: Dict[str, Any]) -> Tuple[List[Dict[str, Any]], List[str]]:
    """
    Complète la partie variations de f dans le tableau de dictionnaires tableau.

    :param tableau: voir introduction
    :param f: la fonction étudiée, afin de calculer les limites et extrema
    :param Df: l'ensemble de définition de f
    :param limites: doit-on afficher les limites dans le tableau ?
    :param extrema: doit-on afficher les extrema dans le tableau ?
    :param latex_parameters: dictionnaire contenant les paramètres d'affichage LaTeX
    :return: un tuple contenant les tableau mis à jour ainsi qu'une liste de consignes permettant de placer les points \
    d'inflexion
    """
    x = Symbol('x')
    tkztabima = []
    for i in range(len(tableau) - 1):
        # ## Borne inférieure ##
        if 'h' in tableau[i]['fp']:
            # i est nécessairement non nul
            # -H/0, si valeur autorisée, -DH/0, si valeur interdite
            if '+' in tableau[i - 1]['fp']:
                tableau[i]['f'] += '+'
            elif '-' in tableau[i - 1]['fp']:
                tableau[i]['f'] += '-'
            if not Df.contains(tableau[i]['value']):
                tableau[i]['f'] += 'D'
            tableau[i]['f'] += 'H/'
            if limites and not Df.contains(tableau[i]['value']):
                tableau[i]['f'] += latex(limit(f, x, tableau[i]['value']), **latex_parameters)
            elif extrema and Df.contains(tableau[i]['value']):
                tableau[i]['f'] += latex(f.subs(x, tableau[i]['value']), **latex_parameters)
            tableau[i]['f'] += ','
        elif not Df.contains(tableau[i]['value']):
            # D-/oo, si début de tableau ou -D+/-oo/-oo, si en cours de tableau
            if i == 0:
                if tableau[i]['value'] != -oo:
                    tableau[i]['f'] += 'D'
                if '+' in tableau[i]['fp']:
                    tableau[i]['f'] += '-/'
                else:
                    tableau[i]['f'] += '+/'
            elif i > 0:
                if '+' in tableau[i - 1]['fp']:
                    tableau[i]['f'] += '+D'
                elif '-' in tableau[i - 1]['fp']:
                    tableau[i]['f'] += '-D'
                if '+' in tableau[i]['fp']:
                    tableau[i]['f'] += '-/'
                elif '-' in tableau[i]['fp']:
                    tableau[i]['f'] += '+/'
                if limites:
                    tableau[i]['f'] += latex(limit(f, x, tableau[i]['value'], '-'), **latex_parameters) + '/' + \
                                       latex(limit(f, x, tableau[i]['value'], '+'), **latex_parameters)
            if i == 0 and limites:
                tableau[i]['f'] += latex(limit(f, x, tableau[i]['value']), **latex_parameters)
            tableau[i]['f'] += ','
        else:
            # -/0, ou R/, et \tkzTabIma{i}{i+2}{i+1}{0}
            if i == 0 and '+' in tableau[i]['fp']:
                tableau[i]['f'] += '-/'
                if extrema:
                    tableau[i]['f'] += latex(f.subs(x, tableau[i]['value']), **latex_parameters)
            elif i == 0 and '-' in tableau[i]['fp']:
                tableau[i]['f'] += '+/'
                if extrema:
                    tableau[i]['f'] += latex(f.subs(x, tableau[i]['value']), **latex_parameters)
            elif i > 0:
                if '+' in tableau[i - 1]['fp']:
                    if '+' in tableau[i]['fp']:
                        tableau[i]['f'] += 'R/'
                        if extrema:
                            tkztabima.append('\\tkzTabIma{%s}{%s}{%s}{%s}' % (
                                i, i + 2, i + 1, latex(f.subs(x, tableau[i]['value']), **latex_parameters)))
                    else:
                        tableau[i]['f'] += '+/'
                        if extrema:
                            tableau[i]['f'] += latex(f.subs(x, tableau[i]['value']), **latex_parameters)
                else:
                    if '-' in tableau[i]['fp']:
                        tableau[i]['f'] += 'R/'
                        if extrema:
                            tkztabima.append('\\tkzTabIma{%s}{%s}{%s}{%s}' % (
                                i + 1, i + 3, i + 2, latex(f.subs(x, tableau[i]['value']), **latex_parameters)))
                    else:
                        tableau[i]['f'] += '-/'
                        if extrema:
                            tableau[i]['f'] += latex(f.subs(x, tableau[i]['value']), **latex_parameters)
            tableau[i]['f'] += ','
        if i == len(tableau) - 2:
            # dernière entrée
            if not Df.contains(tableau[i + 1]['value']):
                if tableau[i + 1]['value'] != oo:
                    if '+' in tableau[i]['fp']:
                        tableau[i + 1]['f'] += '+'
                    else:
                        tableau[i + 1]['f'] += '-'
                    tableau[i + 1]['f'] += 'D/'
                    if limites:
                        tableau[i + 1]['f'] += latex(limit(f, x, tableau[i + 1]['value']), **latex_parameters)
                else:
                    if '+' in tableau[i]['fp']:
                        tableau[i + 1]['f'] += '+/'
                    else:
                        tableau[i + 1]['f'] += '-/'
                    if limites:
                        tableau[i + 1]['f'] += latex(limit(f, x, tableau[i + 1]['value']), **latex_parameters)
            else:
                if '+' in tableau[i]['fp']:
                    tableau[i + 1]['f'] += '+/'
                else:
                    tableau[i + 1]['f'] += '-/'
                if extrema:
                    tableau[i + 1]['f'] += latex(f.subs(x, tableau[i + 1]['value']), **latex_parameters)
    return tableau, tkztabima


def afficheTableau(tableau: List[Dict[str, Any]], tkztabima: List[str], latex_parameters: Dict[str, Any], lgt: float,
                   espcl: float, deltacl: float) -> str:
    """

    :param tableau: voir introduction
    :param tkztabima: iste de consignes permettant de placer les points d'inflexion
    :param latex_parameters: dictionnaire contenant les paramètres d'affichage LaTeX
    :param lgt: largeur de la première colonne
    :param espcl: largeur des colonnes de chaque intervalle
    :param deltacl: Espace ajouté avant le 1er et après le dernier intervalle (pour noter les limites)
    :return: les instructions au format tkz-tab permettant de construire le tableau de variations de f
    """
    texte = r'\begin{tikzpicture}' + '\n'
    texte += r'  \tkzTabInit[lgt=%s,espcl=%s,deltacl=%s]' % (lgt, espcl, deltacl) + '\n'
    colonne1 = r'  {$x$/.6,'
    ligne1 = r'  {'
    signes = r'  \tkzTabLine{'
    derivee = signes
    variations = r'  \tkzTabVar{'
    details: Dict[str, Any] = {}
    for el in tableau:
        for k in el.keys():
            if k == 'value':
                ligne1 += latex(el[k], **latex_parameters) + ','
            elif k == 'f':
                variations += el[k]
            elif k == 'fp':
                derivee += el[k]
            else:
                if k in details.keys():
                    details[k] += el[k]
                else:
                    details[k] = signes + el[k]
                    colonne1 += k + '/.6,'
    if len(details) == 1:
        colonne1 = r'  {$x$/.6,'
    colonne1 += '$f\'(x)$/.6, $f(x)$/1.2'
    texte += colonne1 + '}' + '\n'
    texte += ligne1.rstrip(',') + '}' + '\n'
    if len(details) > 1:
        for k in details.keys():
            texte += details[k] + '}' + '\n'
    texte += derivee + '}' + '\n'
    texte += variations + '}' + '\n'
    for inflexion in tkztabima:
        texte += '  ' + inflexion + '\n'
    texte += r'\end{tikzpicture}' + '\n'
    texte = texte.replace(r'$\infty$', r'$+\infty$')
    return texte


def tabvar(f: str, limites: bool = True, extrema: bool = True, D: Union[str, sets.Set] = S.Reals, lgt: float = 2,
           espcl: float = 2, deltacl: float = .5):
    """
    Programme principal

    :param f: fonction à étudier au format texte, qui sera évaluée par sympy
    :param limites: doit-on afficher les limites dans le tableau ?
    :param extrema: doit-on afficher les extrema dans le tableau ?
    :param D: Ensemble de définition sur lequel on étudie la fonction. Par défaut, on choisit le plus grand possible.
    :param lgt: largeur de la première colonne
    :param espcl: largeur des colonnes de chaque intervalle
    :param deltacl: Espace ajouté avant le 1er et après le dernier intervalle (pour noter les limites)
    :return: Le tableau de variations au format tkz-tab
    """
    if type(D) == str:
        D = eval(D)
    latex_parameters = {'ln_notation': True, 'mode': 'inline', 'root_notation': True,
                        'decimal_separator': 'comma', 'fold_short_frac': False}

    x = Symbol('x')
    f = eval(f)
    fp = diff(f, x).factor()
    Df = calculus.util.continuous_domain(f, x, D)

    tfacteurs = Mul.make_args(fp)
    # Décompose la factorisation pour mettre en facteur la constante et réduire les écritures d'exponentielles
    constante = 1
    exponentielles = 1
    for term in tfacteurs:
        if term.func == exp or term.func == Exp1:
            # Ne factorise pas les exponentielles qui sont toujours strictement positives
            exponentielles *= term
        elif not term.free_symbols:
            constante *= term
    exponentielles = simplify(exponentielles)
    fp = constante * simplify(exponentielles) * (fp / constante / exponentielles).factor(deep=True)
    n, d = fraction(fp / constante / exponentielles)
    if n.simplify() == 1:
        n = ()
    else:
        n = Mul.make_args(n.factor(deep=True))
    if d.simplify() == 1:
        d = ()
    else:
        d = Mul.make_args(d.factor(deep=True))
    if x in n:
        i = n.index(x)
        n = n[:i] + (constante * x,) + n[i + 1:]
        if exponentielles != 1:
            n = (exponentielles,) + n
    elif constante > 0:
        if exponentielles != 1:
            n = (exponentielles,) + n
    else:
        if exponentielles != 1:
            n = (constante, exponentielles,) + n
        else:
            n = (constante,) + n
    tfacteurs = n + d

    dfacteurs = {latex(i, **latex_parameters): '' for i in tfacteurs}
    print(r'\[ f(x) = ', latex(f), '\\qquad f\'(x)=' + latex(fp) + r'\]')

    tableau = ensembleDeDefinition(Df, dfacteurs)
    for el in n:
        tableau = insertRacines(el, tableau, dfacteurs, Df, 'z', latex_parameters)
    for el in d:
        tableau = insertRacines(el, tableau, dfacteurs, Df, 'd', latex_parameters)
    if len(tfacteurs) == 1:
        # On ne détaille pas si il n'y a qu'un signe dans la dérivée. Exemple : f(x)=exp(x-5)
        tfacteurs = ()
    for el in tfacteurs + (fp,):
        tableau = definitSignes(el, tableau, Df, fp, latex_parameters)
    tableau, tkztabima = definitVariations(tableau, f, Df, limites, extrema, latex_parameters)
    return afficheTableau(tableau, tkztabima, latex_parameters, lgt, espcl, deltacl)


# print(tabvar('1/x', limites=True, extrema=True))
