:mod:`pyTkzTab` --- Création d'un tableau de variation
======================================================
Le module :mod:`pyTkzTab` calcule et crée la chaîne de caractères permettant
d'afficher le tableau de variations d'une fonction.

.. automodule:: pyTkzTab
    :members:
    :special-members:
..    :private-members:
      :undoc-members:
      :show-inheritance:
